package com.kshrd.jsoup;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 8/6/17.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieVH> {

    private List<Movie> movieList = new ArrayList<>();
    private MovieRecyclerItemClickListener listener;

    public MovieAdapter(MovieRecyclerItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public MovieVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MovieVH(view);
    }

    @Override
    public void onBindViewHolder(MovieVH holder, int position) {
        holder.onBind(movieList.get(position));
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void addItems(List<Movie> movies){
        this.movieList.addAll(movies);
        notifyDataSetChanged();
    }

    class MovieVH extends RecyclerView.ViewHolder {

        TextView tvTitle, tvDetail, tvTrailer;
        ImageView ivThumbnail;

        public MovieVH(View itemView) {
            super(itemView);
            tvDetail = (TextView) itemView.findViewById(R.id.tvDeTail);
            tvTitle = (TextView) itemView.findViewById(R.id.tvtitle);
            tvTrailer = (TextView) itemView.findViewById(R.id.tvTrailer);
            ivThumbnail = (ImageView) itemView.findViewById(R.id.ivThumbnail);

            tvDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onDetailClicked(movieList.get(getAdapterPosition()).getDetail());
                }
            });

            tvTrailer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onTrailerClicked(movieList.get(getAdapterPosition()).getTrailer());
                }
            });
        }

        public void onBind(Movie movie) {

            tvTitle.setText(movie.getTitle());
            Picasso
                    .with(ivThumbnail.getContext())
                    .load(movie.getThumbnail())
                    .into(ivThumbnail);

            if (movie.getTrailer() == null){
                tvTrailer.setVisibility(View.GONE);
            } else {
                tvTrailer.setVisibility(View.VISIBLE);
            }
        }
    }

}
