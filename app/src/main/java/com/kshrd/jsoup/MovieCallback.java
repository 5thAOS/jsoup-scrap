package com.kshrd.jsoup;

import java.util.List;

/**
 * Created by pirang on 8/6/17.
 */

public interface MovieCallback {

    void onPreExecute();
    void onPostExecute(List<Movie> movieList);

}
