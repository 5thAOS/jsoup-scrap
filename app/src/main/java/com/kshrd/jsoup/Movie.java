package com.kshrd.jsoup;

/**
 * Created by pirang on 8/6/17.
 */

public class Movie {

    private String title;
    private String thumbnail;
    private String date;
    private String detail;
    private String trailer;

    public Movie(String title, String thumbnail, String date, String detail, String trailer) {
        this.title = title;
        this.thumbnail = thumbnail;
        this.date = date;
        this.detail = detail;
        this.trailer = trailer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", date='" + date + '\'' +
                ", detail='" + detail + '\'' +
                ", trailer='" + trailer + '\'' +
                '}';
    }
}
