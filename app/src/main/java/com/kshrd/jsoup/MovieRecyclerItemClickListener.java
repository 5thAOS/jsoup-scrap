package com.kshrd.jsoup;

/**
 * Created by pirang on 8/6/17.
 */

public interface MovieRecyclerItemClickListener {

    void onDetailClicked(String url);

    void onTrailerClicked(String url);

}
