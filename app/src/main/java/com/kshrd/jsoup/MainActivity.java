package com.kshrd.jsoup;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MovieCallback, MovieRecyclerItemClickListener {

    RecyclerView rvMovie;
    MovieAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvMovie = (RecyclerView) findViewById(R.id.rvMovie);
        rvMovie.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MovieAdapter(this);
        rvMovie.setAdapter(adapter);

        new CinemaParser(this).execute();


    }

    @Override
    public void onPreExecute() {

    }

    @Override
    public void onPostExecute(List<Movie> movieList) {
        adapter.addItems(movieList);
        for (Movie m: movieList){
            Log.e("ooooo", m.getThumbnail());
        }
    }

    @Override
    public void onDetailClicked(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onTrailerClicked(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);
    }
}
